$(document).ready(function() {
        console.group('youtube-dw-buttons');
        (function() {
            var n = location.href.match(new RegExp('^https?://(www\\.)?youtube\\.com/watch\\?(.+)$'));
            if (typeof(n) != 'object') { return; }

            var url_get = (n[2] + '&').split('&');
            var video_id = null;
            for (var i = 0; i < url_get.length; i++) {
                var j = url_get[i].match(new RegExp('^v\\=([0-9a-zA-Z_-]+)$'));
                if (j !== null) {
                    video_id = j[1];
                    break;
                }
            }

            if (video_id === null) {
                return;
            }

            // А теперь разбираем
            $ajax({
                'url': 'https://www.youtube.com/get_video_info?video_id=' + video_id +
                       '&asv=3&eurl=https%3A%2F%2Fvk.com%2F&el=embedded',
                'success': function(raw) {
                    var links = parse_youtube_answer(raw);
                    console.log(links);
                    create_links(links);
                }
            });

        })();
        console.groupEnd();
    }
);

/**
 * Упёрто из VK OPT
 *
 * @param {string} raw
 * @return {object}
 */
function YTDataDecode(raw) {
    if (!raw) {
        return {};
    }
    var exclude = {'url': 1, 'type': 1, 'ttsurl': 1};// !='url' && key!='type'
    var query = {}, dec = function(str) {
        try {
            return decodeURIComponent(str);
        } catch (e) { return str; }
    };

    var qa = raw.split('&');
    for (var i = 0; i < qa.length; i++) {
        var t = qa[i].split('=');
        if (t[0]) {
            var key = dec(t[0]);
            var v = exclude[key] ? [dec(t[1] + '')] : dec(t[1] + '').split(',');
            query[key] = [];
            for (var j = 0; j < v.length; j++) {
                if (v[j].indexOf('&') != -1 && v[j].indexOf('=') != -1 && !exclude[key]) {
                    v[j] = YTDataDecode(v[j]);
                }
                query[key].push(v[j]);
            }
            if (query[key].length == 1) {
                query[key] = query[key][0];
            }
        }
    }
    return query;
}

//YouTube formats list
var YT_video_itag_formats = {

    '0': '240p.flv',
    '5': '240p.flv',
    '6': '360p.flv',
    '34': '360p.flv',
    '35': '480p.flv',

    '13': '144p.3gp (small)',
    '17': '144p.3gp (medium)',
    '36': '240p.3gp',

    '160': '240p.mp4 (no audio)',
    '18': '360p.mp4',
    '135': '480p.mp4 (no audio)',
    '22': '720p.mp4',
    '37': '1080p.mp4',
    '137': '1080p.mp4 (no audio)',
    '38': '4k.mp4',
    '82': '360p.mp4',//3d?
    //'83': '480p.mp4',//3d?
    '84': '720p.mp4',//3d?
    //'85': '1080p.mp4',//3d?

    '242': '240p.WebM (no audio)',
    '43': '360p.WebM',
    '44': '480p.WebM',
    '244': '480p.WebM (low, no audio)',
    '45': '720p.WebM',
    '247': '720p.WebM (no audio)',
    '46': '1080p.WebM',
    '248': '1080p.WebM (no audio)',
    '100': '360p.WebM',//3d?
    //'101':'480p.WebM',//3d?
    '102': '720p.WebM',//3d?
    //'103':'1080p.WebM',//3d?

    '139': '48kbs.aac',
    '140': '128kbs.aac',
    '141': '256kbs.aac',

    '171': '128kbs.ogg',
    '172': '172kbs.ogg'
};


/**
 *
 * @param {string} raw Сырой ответ
 * @returns {String[]|null}
 */
function parse_youtube_answer(raw) {
    var obj = YTDataDecode(raw);
    //noinspection JSUnresolvedVariable
    var map = (obj.fmt_url_map || obj.url_encoded_fmt_stream_map);
    if (!map) {return null;}

    var links = [];
    for (var i = 0; i < map.length; i++) {
        //noinspection JSUnresolvedVariable
        var sig = map[i].sig;
        //noinspection JSUnresolvedVariable
        if (!map[i].sig && map[i].s) {
            continue;
        }// "Qi(map[i].s)" calc sig normaly, but links not valid

        //noinspection JSUnresolvedVariable
        var format = YT_video_itag_formats[map[i].itag];
        //noinspection JSUnresolvedVariable
        var info = (map[i].type + '').split(';')[0] + ' ' + (obj.fmt_list[i] + '').split('/')[1];
        if (!format) {
            //noinspection JSUnresolvedVariable
            console.log('<b>YT ' + map[i].itag + '</b>: \n' + (map[i].stereo3d ? '3D/' : '') + info, 1);
        }
        //noinspection JSUnresolvedVariable
        format = (map[i].stereo3d ? '3D/' : '') + (format ? format : info);
        obj.title = (Array.isArray(obj.title)) ? obj.title.join('') : obj.title;
        //noinspection JSUnresolvedVariable
        links.push([map[i].url + '&signature=' + sig + '&quality=' + map[i].quality +
                    (obj.title ? '&title=' + encodeURIComponent(obj.title.replace(/\+/g, ' ')) : ''), format,
                    info]);
    }
    return links;
}

/**
 * Создаём элементы в контейнере
 *
 * @param {String[]} links Список спарсенных ссылок
 */
function create_links(links) {
    var container = document.createElement('div');
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        var a = document.createElement('a');
        $(a).addClass('link').attr({
            'href': link[0],
            'target': '_blank'
        }).text(link[2]);

        container.appendChild(a);
    }

    $(container).addClass('dw-buttons-container');
    $('#watch7-user-header').append(container);
}